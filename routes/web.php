<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/dashboard', 'DashboardController@view_Dashboard')->name('viewdashboard');

Route::get('/sendemail', 'SendEmailController@index')->name('gotosendemail');
Route::post('/sendemail/send', 'SendEmailController@sendemail')->name('sendemail');

Route::get('/filedownload', function(){
    return view('filedownload');
});

/* Route::get('/emailmanager', function(){
    return view('email_manager');
}); */

Route::get('/emailmanager', 'Email_manager_Controller@index')->name('emailmanager_createnew');
Route::post('/emailmanager/store', 'Email_manager_Controller@store')->name('emailmanager_store');
Route::get('/ajaxcheckemailinfo', 'Email_manager_Controller@checkemailinfo');

/* Route::get('/emailscheduler', function(){
    return view('email_scheduler');
}); */

Route::get('/emailscheduler', 'Email_scheduler_Controller@index')->name('emailscheduler_new');
Route::get('/ajax_get_batch_info', 'Email_scheduler_Controller@get_batch_info');
Route::post('/emailsend/send', 'Email_scheduler_Controller@emailsend')->name('emailsend');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
