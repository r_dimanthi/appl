  @extends('layouts.app')

  @section('style')

  @endsection
  
  @section('content') 

    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Email manager
        <small>welcome</small>
      </h1>
      <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <!-- <h3 class="box-title">Create</h3>

           <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div> -->
        </div>
        <div class="box-body">
          
              @if(count($errors) > 0)

        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <ul>
                @foreach ($errores->all() as $error)
                    <li>{{ $error }}</li>                    
                @endforeach
            </ul>
        </div>

    @endif

    @if($message = Session::get('success'))

        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong>{{ $message }}</strong>
        </div>

    @endif

    <div class="row">
                           <div class="col-sm-12">
                                <div id="error_box" class="alert alert-danger" hidden>
                                 <!-- validations errors will be added here -->
                                </div>
                                <div id="msg_box" class="alert alert-success" hidden>
                                 <!-- messages will be added here -->
                                </div>
                           </div>
    </div>

  <form method="post" id="form_emn" action="{{ url('emailmanager/store') }}" enctype="multipart/form-data" >

    {!! csrf_field() !!}

    <div class="form-group">
        <label for="brand_wise" class="form-control-label"><b style="color: red;">*</b> Select an option:</label>
        <div class="input-group">
        <div class="col-md-6">
        <input type="radio" name="batch_type" id="new" value="n"> Create new batch
        </div>
        <div class="col-md-6">
        <input type="radio" name="batch_type" id="existing" value="e"> Select from existing batch
        </div>
        </div>
    </div>

    <div class="panel-group">
        <div class="panel panel-default" id="pnl_new_batch" hidden>
          <div class="panel-body">
              <div class="form-group col-md-4">
                  <label><b style="color: red;">*</b>Batch name</label>
                  <input type="text" name="text_batch" id="text_batch" class="form-control" />
              </div>
              <div class="form-group col-md-12">
                  <label><b style="color: red;">*</b>Upload document</label>
                  <input type="file" name="upload_file" id="upload_file" >
              </div>
              <div class="row m-t-20">
                    <div class="col-lg-3" >
                      <div class="form-group ">
                        <label for="destinationemail" class="form-control-label">&nbsp;Enter destination Email Address</label>
                        <div class="input-group ">
                          <input type="text" name="b_new_email[]" id="b_new_email" placeholder="Enter destination Email"  class="form-control address_email newe">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-1 m-t-35" >
                    <label>&nbsp;</label>
                      <span class="btn btn-primary" id="btn_receiver_add1">ADD</span>
                    </div>

                    <div class="col-lg-1 m-t-35" ></div>

                  </div>

                  <div id="field_wrapper1"></div>
                    
                  <br>

          </div>
        </div>
        <div class="panel panel-default" id="pnl_existing_batch" hidden>
          <div class="panel-body">
                <div class="form-group ">
                  <label for="select_batch" class="form-control-label"><b style="color: red">*</b>&nbsp;
Select Batch name and code:</label>
                    <div class="input-group">           
                      <select class="form-control chzn-select" name="select_batch" id="select_batch" >
                                <option selected disabled="disabled" value="-1">Batch code-name</option>
                            @foreach ($batchname_code as $bnc)
                                <option value="{{$bnc->id}}">{{$bnc->batchcode."-".$bnc->batch_name}}</option>
                            @endforeach
                      </select>
                    </div>
                </div>
            <div class="row m-t-20">
                    <div class="col-lg-3" >
                      <div class="form-group ">
                        <label for="destinationemail" class="form-control-label">Enter destination Email Address</label>
                        <div class="input-group ">
                          <input type="text" name="b_exist_email[]" id="b_exist_email" placeholder="Enter destination Email"  class="form-control address_email existe">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-1 m-t-35" >
                    <label>&nbsp;</label>
                      <span class="btn btn-primary" id="btn_receiver_add2">ADD</span>
                    </div>

                    <div class="col-lg-1 m-t-35" ></div>

                  </div>

                  <div id="field_wrapper2"></div>
                    
                  <br>

          </div>
        </div>
    </div>
    <div class="form-group col-lg-12" hidden id="process_btns">
      <div class="col-md-2">
          <input type="submit" name="submit" id="submit" class="btn btn-info">
      </div>
      <div class="col-md-2">
          <input type="button" name="cancel" id="cancel" class="btn btn-warning" value="cancel">
      </div>
    </div>    

  </form>

        </div>
        <!-- /.box-body -->
        <!-- <div class="box-footer">
          Footer
        </div> -->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @endsection

  @section('script') 

  <script>

      var email_check = "0";

      $(document).ready(function() {

        //$("#error_box").hide();

        var x=1;
        var y=1;
        show_form1(x);
        show_form2(y);        

        //alert("12345");
          $('input[type=radio][name=batch_type]').change(function() {
            $("#process_btns").show();
            if (this.value == 'n') {
                console.log("New batch");
                $("#pnl_new_batch").show();
                $("#pnl_existing_batch").hide();
            }
            else if (this.value == 'e') {
                console.log("Existing batch");
                $("#pnl_existing_batch").show();
                $("#pnl_new_batch").hide();
            }
          }); 
      });  


  function show_form1($idx)
  {
        /*add the multiple rows for one input field(location)*/
        var maxfield = 100;
        //var addButton = $('#add_button1');
        var wrapper = $('#field_wrapper1');
        var fieldHTML ='<div class="row m-t-10 "><div class="col-lg-3 "><div class="form-group"><div class="input-group "><input type="text" name="b_new_email[]" id="b_new_email" placeholder="" class="form-control address_email" ></div></div></div><div class="col-lg-1" ><button class="btn btn-danger remove_button1">REMOVE</button></div></div>';

        //var x=$idx;
        $("#btn_receiver_add1").click(function(){
      
          if($idx<maxfield){
          /* alert ($idx);*/
            $(wrapper).append(fieldHTML);
            $idx++;
          }

          $(".address_email").focusout(function(){
               var email = this.value;
               console.log(email);
               checkEmailInfo(email);
            });
        });

        $(wrapper).on('click','.remove_button1', function(e){
          e.preventDefault();
          $(this).parent('div').parent('div').remove();
          $idx--;
        });

  }

    function show_form2($idy)
  {
        /*add the multiple rows for one input field(location)*/
        var maxfield = 100;
        //var addButton = $('#add_button1');
        var wrapper = $('#field_wrapper2');
        var fieldHTML ='<div class="row m-t-10 "><div class="col-lg-3 "><div class="form-group"><div class="input-group "><input type="text" name="b_exist_email[]" id="b_exist_email" placeholder="" class="form-control address_email" ></div></div></div><div class="col-lg-1" ><button class="btn btn-danger remove_button2">REMOVE</button></div></div>';

        //var x=$idx;
        $("#btn_receiver_add2").click(function(){
      
          if($idy<maxfield){
          /* alert ($idx);*/
            $(wrapper).append(fieldHTML);
            $idy++;
          }
            $(".address_email").focusout(function(){
               var email = this.value;
               console.log(email);
               checkEmailInfo(email);
            });
        });

        $(wrapper).on('click','.remove_button2', function(e){
          e.preventDefault();
          $(this).parent('div').parent('div').remove();
          $idy--;
        });

  }

  $(".address_email").focusout(function(){
        var email = this.value;
        console.log(email);
        checkEmailInfo(email);
        /*alert("data: "+ email_check);
        if(a == 1){
          $(this).val("");
        }*/
  });

  function checkEmailInfo(email_address){
    //var info="0";
    if(IsEmail(email_address) == true){
              $.ajax({

          type: "GET",
          url: "{{URL::to('/ajaxcheckemailinfo')}}",
          data: {email:email_address},
          cache: false, 

          success: function(data){ 
              //alert(data);

              if(data == "0"){
                $("#error_box").hide();

              }else if(data == "1"){
                //info = "1";
                //alert(email_check);
                $("#error_box").show();
                $('#error_box').html('This email address has already been allocated by another user.Please ensert another email');
                
              }else{}
          }
        });
    }else{
                $("#error_box").show();
                $('#error_box').html('Please enter a valid email address');
    }
      //return info;
    }

      function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
          return false;
        }else{
          return true;
        }
      }

      // process the form
    $('form').submit(function(event) {
        event.preventDefault();

        var valu = validation();
        console.log("validation result:"+ valu);
        if(valu == 1){
                $.ajax({
                type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                url         : "{{URL::to('emailmanager/store')}}", // the url where we want to POST
                data        : new FormData(this), // our data object
                dataType    : 'JSON',
                contentType : false, // what type of data do we expect back from the server
                cache : false,
                processData : false,
                success: function(data){
                  console.log("data:"+data.message);
                  if(data.type == 'success'){
                    $("#msg_box").show();
                    $('#msg_box').html(data.message);
                  }else{
                    $("#error_box").show();
                    $('#error_box').html(data.message);
                  }
                }

            });
        }else{
                $("#error_box").show();
                $('#error_box').html('Please fill all the required fields');
        }
    });

    function validation(){
      var v_batch_type = $("input[name='batch_type']:checked").val();
      var v_text_batch = $("#text_batch").val();  

      console.log(v_batch_type);

      if(v_batch_type == 'n'){

        if (v_text_batch.length === 0){
          return -1;
        }
         if( document.getElementById("upload_file").files.length == 0 ){
          return -1;
        }
        return 1;

      }else if(v_batch_type == 'e'){

        if($("#select_batch option:selected").val() == "-1"){
            return -12;
        }

        $("input[name='b_exist_email[]']").each(function() {
          if ($(this).val().length === 0) {
              return -13;
          }
        }); 

        return 1;

      }else{
          return -11;
      }
    }

      $('#cancel').click(function(event) {
        console.log("cancel button click");
        document.getElementById("form_emn").reset();
      });
  </script>

  @endsection