  @extends('layouts.app')

  @section('style')

  @endsection
  
  @section('content') 

    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Blank page
        <small>it all starts here</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          
              @if(count($errors) > 0)

        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <ul>
                @foreach ($errores->all() as $error)
                    <li>{{ $error }}</li>                    
                @endforeach
            </ul>
        </div>

    @endif

    @if($message = Session::get('success'))

        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong>{{ $message }}</strong>
        </div>

    @endif

  <form method="post" action="{{ url('sendemail/send') }}" enctype="multipart/form-data" >

    {!! csrf_field() !!}
    
    <div class="form-group">
        <label>Enter your name</label>
        <input type="text" name="uname" id="uname" class="form-control" />
    </div>
    <div class="form-group">
        <label>Enter destination Email Address</label>
        <input type="text" name="email" id="email" class="form-control" />
    </div>
    <div class="form-group">
        <label>Enter your message</label>
        <textarea name="message" id="message" class="form-control" ></textarea>
    </div>
    <div class="form-group">
        <label>Upload document</label>
        <input type="file" name="upload_file" id="upload_file" >
    </div>
    <div class="form-group">
        <input type="submit" name="submit" id="submit" class="btn btn-info">
    </div>

  </form>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @endsection

  @section('script') 

  @endsection