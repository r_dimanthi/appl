    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <!-- <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a> -->
          <ul class="treeview-menu">
            <li class="active"><a href='/dashboard'><i class="fa fa-circle-o"></i> Dashboard</a></li>
          </ul>
        </li>       
        <!-- <li>
          <a href="{{ asset('adminlte/pages/mailbox/mailbox.html') }}">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-yellow">12</small>
              <small class="label pull-right bg-green">16</small>
              <small class="label pull-right bg-red">5</small>
            </span>
          </a>
        </li> -->
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Email operations</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/filedownload') }}"><i class="fa fa-circle-o"></i> File download</a></li>
            <li><a href="{{ url('/emailmanager') }}"><i class="fa fa-circle-o"></i> Email manager</a></li>
            <li><a href="{{ url('/emailscheduler') }}"><i class="fa fa-circle-o"></i> Email scheduler</a></li>
            <!-- <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> email-test1
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li> -->
          </ul>
        </li>
        <!-- <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li> -->        
      </ul>
    </section>
    <!-- /.sidebar -->