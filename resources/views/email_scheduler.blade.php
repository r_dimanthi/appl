  @extends('layouts.app')

  @section('style')

  @endsection
  
  @section('content') 

    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Email Scheduler
        <small>welcome</small>
      </h1>
      <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <!-- <h3 class="box-title">Create</h3>

           <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div> -->
        </div>
        <div class="box-body">
          
              @if(count($errors) > 0)

        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <ul>
                @foreach ($errores->all() as $error)
                    <li>{{ $error }}</li>                    
                @endforeach
            </ul>
        </div>

    @endif

    @if($message = Session::get('success'))

        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong>{{ $message }}</strong>
        </div>

    @endif

  <form method="post" id="form_esc" action="{{ url('emailsend/send') }}" enctype="multipart/form-data" >

    {!! csrf_field() !!}

                <div class="row">
                    <div class="col-md-4 form-group ">
                      <label for="select_batch" class="form-control-label"><b style="color: red">*</b>&nbsp;
    Select Batch name and code:</label>
                        <div class="input-group">           
                          <select class="form-control chzn-select" name="select_batch" id="select_batch" >
                                    <option selected disabled="disabled" value="-1">Batch code-name</option>
                                @foreach ($batchname_code as $bnc)
                                    <option value="{{$bnc->id}}">{{$bnc->batchcode."-".$bnc->batch_name}}</option>
                                @endforeach
                          </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 form-group" id="attachment">
                  <label>Attachments:</label>
                  
                </div>
                <input type="hidden" name="file_subpath" id="file_subpath">
                <div class="col-md-4 form-group" id="email_count">
                  <label>Number of emails:</label>
              </div>
              <div class="row">
                  <!--<div class="col-md-4 form-group">
                      <label>Email alises:</label>
                      <input type="text" name="alises" id="alises" class="form-control" />
                  </div>-->
                  <div class="col-md-4 form-group">
                      <label>Subject:</label>
                      <input type="text" name="subject" id="subject" class="form-control" />
                  </div>
                  <div class="col-md-4 form-group">
                      <label>Send date:</label>
                      <input type="datetime-local" name="datetime" id="datetime" class="form-control" />
                  </div>
              </div>
              <div class="col-md-10 form-group">
                  <label>Content:</label>
                  <textarea name="emailbody" id="emailbody" rows="10" cols="100"  maxlength="5000">
                  
                  </textarea>
              </div>
              <br>
              <br>

    <div class="form-group col-lg-12">
      <div class="col-md-2">
          <input type="submit" name="submit" id="submit" class="btn btn-info">
      </div>
      <div class="col-md-2">
          <input type="button" name="cancel" id="cancel" class="btn btn-warning" value="cancel">
      </div>
    </div>
    

  </form>

        </div>
        <!-- /.box-body -->
        <!-- <div class="box-footer">
          Footer
        </div> -->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @endsection

  @section('script') 

  <script>

      $(document).ready(function() {
        
          $('#select_batch').change(function() {
            var batch_id = this.value;
              $.ajax({

                type: "GET",
                url: "{{URL::to('/ajax_get_batch_info')}}",
                data: {batch_id:batch_id},
                cache: false, 
                dataType: 'json',

                success: function(data){ 
                    console.log(data['batch_info']['filepath']);
                    console.log(data['batch_info']['batchcode']);
                    console.log(data['num_of_emails']);

                    var filepath = data['batch_info']['filepath'];
                    var batchcode = data['batch_info']['batchcode'];
                    var spath = batchcode+'/'+filepath;
                    //var appurl = {!! json_encode(url('/')) !!};
                    //var add = appurl+'/public/uploads/email_uploads/'+batchcode+'/'+filepath;
                    //console.log(appurl);
                    //console.log(add);

                    $("#email_count").html('<label>Number of emails: '+ data['num_of_emails'] +'</label>');
                    $("#attachment").html('<label>Attachments: '+ filepath +'</label>');
                    $("#file_subpath").val(spath);
                }
              });
          }); 
      });  

      $('#cancel').click(function(event) {
        console.log("cancel button click");
        document.getElementById("form_esc").reset();
      });

   /* $('#submit').click(function(event) {
        event.preventDefault();

        var valu = validation();
        console.log("validation result:"+ valu);
        if(valu == 1){
            console.log("vdfgvg");
              $('form').submit();

                $.ajax({
                type        : 'POST', 
                url         : "{{URL::to('emailsend/send')}}", 
                data        : new FormData(this), 
                dataType    : 'JSON',
                contentType : false, 
                cache : false,
                processData : false,
                success: function(data){
                  console.log("data:"+data.message);
                  if(data.type == 'success'){
                    $("#msg_box").show();
                    $('#msg_box').html(data.message);
                  }else{
                    $("#error_box").show();
                    $('#error_box').html(data.message);
                  }
                }

            }); 
        }else{
                $("#error_box").show();
                $('#error_box').html('Please fill all the required fields');
        }
    });*/

    function validation(){

      var v_batch = $("#select_batch option:selected").val();
      var v_subject = $("#subject").val();  
      var v_date = $("#datetime").val();
      var v_content = $("#emailbody").html();

      console.log("text area content: " + v_content);

        if(v_batch == "-1"){
            return -1;
        }

        if (v_subject.length === 0){
          return -1;
        }

        if (v_date.length === 0){
          return -1;
        }

        if (v_content.length === 0){
          return -1;
        }
        
        return 1; 

    }

  </script>

  @endsection