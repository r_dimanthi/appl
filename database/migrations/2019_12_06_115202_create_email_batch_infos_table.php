<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailBatchInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_batch_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('batchid');
            $table->string('email_alises',50)->nullable();
            $table->string('test_subject',50)->nullable();
            $table->integer('number_of_emails')->nullable();
            $table->dateTime('send_date')->nullable();
            $table->text('email_body')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_batch_infos');
    }
}
