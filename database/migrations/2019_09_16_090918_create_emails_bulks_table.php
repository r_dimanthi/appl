<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsBulksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails_bulks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('alises',20);
            $table->string('subject',50);
            $table->text('body');
            $table->string('attachment_original_name',75);
            $table->string('attachment_renamed',75);
            $table->timestamp('email_sent_at');
            $table->string('sender_email',150);
            $table->integer('user_id');
            $table->integer('status')->default(0);
            $table->timestamps();

            //$table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails_bulks');
    }
}
