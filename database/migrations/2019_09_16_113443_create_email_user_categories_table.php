<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailUserCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_user_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('emai_id');
            $table->integer('user_category_id');
            $table->timestamps();

            //$table->foreign('emai_id')->references('id')->on('emails_bulks');
            //$table->foreign('user_category_id')->references('id')->on('user_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_user_categories');
    }
}
