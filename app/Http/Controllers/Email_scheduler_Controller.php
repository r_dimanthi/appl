<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Batch;
use App\Batch_email;
use App\Email_batch_info;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use File;
use Illuminate\Support\Facades\Auth;
use Validator;

class Email_scheduler_Controller extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::id();
        $batchname_code = Batch::where('userid', '=', $user)
                          ->where('send_status', '=', 0)
                          ->select('id','batch_name','batchcode')
                          ->get();  
        //return $batchname_code;
        return view('email_scheduler',compact('batchname_code'));
    }

    function get_batch_info(Request $request){
        $batch_id = $request->batch_id;

        $batch_info = Batch::where('id', '=', $batch_id)
        ->select('filepath','batchcode')
        ->first();
        
        $num_of_emails = Batch_email::join('batches', 'batch_emails.batchid', '=', 'batches.id')
        ->where('batches.id', '=', $batch_id)
        ->count();

        $data["batch_info"] = $batch_info;
        $data["num_of_emails"] = $num_of_emails;

        return response()->json($data);
    }

    function emailsend(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'select_batch' => 'required',
            'subject' => 'required',
            'datetime' => 'required',
            'emailbody' => 'required'
        ] );

        if($validation->passes()){
            
            $batch = $request->select_batch;
            $esubject = $request->subject;
            $esenddate = $request->datetime;
            $eemailbody = $request->emailbody;
    
            //return $eemailbody;
    
            $folder_path=public_path().'/uploads/email_uploads/';
            $subpath = $request->file_subpath;
            $file = "";
            
                $data = array(
                    'esubject' => $esubject,
                    'esenddate' => $esenddate,
                    'eemailbody' => $eemailbody,
                    'folder_path' => $folder_path,
                    'subpath' => $subpath
                );
    
                //$receivers = ['rmahawewa@yahoo.com','mahawewaaccravini@gmail.com'];
                $receivers = Batch_email::where('batchid', '=', $batch)
                            ->select('email')
                            ->get();
    
                $ereceivers = array();
    
                foreach($receivers as $r){
                    $eml = $r->email;
                    array_push($ereceivers,$eml);
                }
    
            Mail::to('smrdmahawewa@gmail.com')->cc($ereceivers)->queue(new SendMail($data));
    
            $table = Batch::find($batch); 
            $table->send_status = 1;
            $table->save();
    
            $id = Email_batch_info::where('batchid', '=', $batch)
                    ->select('id')
                    ->first(); 
    
            $table1 = Email_batch_info::find($id->id); 
            $table1->test_subject = $esubject;
            $table1->send_date = $esenddate;
            $table1->email_body = $eemailbody;
            $table1->save();
    
            return back()->with('success', 'Emails sended successfully'); 

        /*    return response()->json([
                'message' => 'All the emails has been sent successfully',
                'type' => 'success'
            ]); */

        }else{

            return back()->with('Error', 'Failed to complete the task. Please try again'); 

        /*    return response()->json([
                'message' => 'Failed to complete the task. Please try again',
                'type' => 'error'
            ]);  */

        }
    }
}
