<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Batch;
use App\Batch_email;
use App\Email_batch_info;
use Carbon\Carbon;
use File;
use Illuminate\Support\Facades\Auth;
use Validator;

class Email_manager_Controller extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::id();
        $batchname_code = Batch::where('userid', '=', $user)
                          ->where('send_status', '=', 0)
                          ->select('id','batch_name','batchcode')
                          ->get();  
        //return $batchname_code;
        return view('email_manager',compact('batchname_code'));
    }

    function store(Request $request)
    {
        /*$this->validate($request, [
            'uname' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]); */ 

        $validation = Validator::make($request->all(),[
            'batch_type' => 'required'
        ] );

        $validation1 = Validator::make($request->all(),[
            'text_batch' => 'required',
            'b_new_email' => 'required',
            'upload_file' => 'required'
        ]);

        $validation2 = Validator::make($request->all(),[
            'select_batch' => 'required'
        ]); 

        if($validation->passes()){

            $batchtype = $request->batch_type;

            if($batchtype == "n"){

                if($validation1->passes()){
               //dd($batchtype);
               $code = $this->generate_code();
   
               $text_batch = $request->text_batch;
               $b_new_email = $request->b_new_email;
   
               $folder_path=public_path().'/uploads/email_uploads/'.$code.'/';
               $user = Auth::id();
   
               $file = "";
               $file_name = "";
               
                   if(File::exists($folder_path)) {
                                   
                       if($request->hasFile('upload_file')){
   
                           $file = $request->file('upload_file');
                           $file_name = $file->getClientOriginalName();
                           //return  $file_name;
   
                           $file->move($folder_path,$file->getClientOriginalName());
                       // return 1;
                       
                       }
                   }else{   
                       File::makeDirectory($folder_path,0775,true);
                       if($request->hasFile('upload_file')){
   
                           $file = $request->file('upload_file');
                           $file_name = $file->getClientOriginalName();
                           //return  $file_name;
   
                           $file->move($folder_path,$file->getClientOriginalName());
                       // return 1;
                       
                       }
                   }
   
                   $table1 = new Batch(); 
                   $table1->batch_name = $text_batch;                  
                   $table1->batchcode = $code; 
                   $table1->userid= $user;
                   $table1->filepath= $file_name;       
                   $table1->save();
   
                   $batch_id=$table1->id;                 
   
               $new_email_count = count($b_new_email);
                   for($i=0; $i<$new_email_count;$i++){
   
                       $info = $this->checkemail($b_new_email[$i]);
                           if(!($info > 0)){
                               $table2 = new Batch_email();
                               $table2->batchid = $batch_id;
                               $table2->email = $b_new_email[$i];
                               $table2->save();
                           }           
                       
                   }
                   $table3 = new Email_batch_info();
                   $table3->batchid = $batch_id;
                   $table3->save();

                }
   
           }elseif($batchtype == "e"){
                if($validation2->passes()){
                    $select_batch = $request->select_batch;
                    $b_exist_email = $request->b_exist_email; //
                    $b_exist_email_count = count($b_exist_email);
                        for($i=0; $i<$b_exist_email_count;$i++){
        
                            $info = $this->checkemail($b_exist_email[$i]);
                                if(!($info > 0)){
                                    $table4 = new Batch_email();
                                    $table4->batchid = $select_batch;
                                    $table4->email = $b_exist_email[$i];
                                    $table4->save();
                                }  
                        }
                }
            }else {
               # code...
            }

           return response()->json([
            'message' => 'Email manager information generated successfully',
            'type' => 'success'
        ]);
            
        }else{
            return response()->json([
                'message' => 'Failed to generate the information. Please try again',
                'type' => 'error'
            ]);
        }


        //return back()->with('success', 'Email manager information generated successfully'); 
        
    }

    private function generate_code(){
        $datetimenow = Carbon::now();
        $datetimenow = $datetimenow->toDateTimeString();

        $datetimenow = preg_replace("/[^0-9]/", "", $datetimenow);
        //dd($datetimenow); 

        $rand_num = rand(111,999);
        //dd($rand_num);

        $code = $rand_num.$datetimenow;
        return $code;
    }

    function checkemailinfo(Request $request){
        $email_address = $request->email;

        $res = $this->checkemail($email_address);
        return response()->json($res);
    }

    function checkemail($email){
        $email_address = $email;
        $user = Auth::id();

        $info = Batch_email::join('batches', 'batch_emails.batchid', '=', 'batches.id')
        ->where('batches.userid', '!=', $user)
        ->where('batch_emails.email', '=', $email_address)
        ->select('batch_emails.id')
        ->get();
        $res = 0;
        
        if(count($info) > 0){
            $res = 1;
        }
        return $res;
    }

}