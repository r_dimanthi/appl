<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use File;

class SendEmailController extends Controller
{
    //
    function index()
    {
        return view('send_email');
    }

    function sendemail(Request $request)
    {
        $this->validate($request, [
            'uname' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        $folder_path=public_path().'/uploads/email_uploads/';
        $file = "";
        
        if(File::exists($folder_path)) {
                          
            if($request->hasFile('upload_file')){

                $file = $request->file('upload_file');
                //return $files->getClientOriginalName();

                $file->move($folder_path,$file->getClientOriginalName());
              // return 1;
              
            }
        }else{   
            File::makeDirectory($folder_path,0775,true);
            if($request->hasFile('upload_file')){

                $file = $request->file('upload_file');
                //return $files->getClientOriginalName();

                $file->move($folder_path,$file->getClientOriginalName());
              // return 1;
              
            }
        }

            $data = array(
                'uname' => $request->uname,
                'email_to' => $request->email,
                'message' => $request->message,
                'folder_path' => $folder_path,
                'file_name' => $file->getClientOriginalName()
            );

            $receivers = ['rmahawewa@yahoo.com','mahawewaaccravini@gmail.com'];

            //return(new SendMail($data));

        //Mail::to($request->email)->send(new SendMail($data));
        //Mail::to($request->email)->cc($moreUsers)->queue(new SendMail($data));
        Mail::to('smrdmahawewa@gmail.com')->cc($receivers)->queue(new SendMail($data));
        return back()->with('success', 'Thanks all'); 


    }
}
